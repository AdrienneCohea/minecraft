resource "google_storage_bucket" "backup" {
  name          = var.backup_bucket_name
  location      = var.backup_bucket_location
}

resource "google_storage_bucket_iam_binding" "backup" {
  bucket = google_storage_bucket.backup.name
  role = "roles/storage.admin"
  members = [
    "serviceAccount:${google_service_account.backup.email}",
  ]
}

resource "google_service_account" "backup" {
  account_id   = "minecraft-backup"
  display_name = "Minecraft Backup"
  description  = "Backup and restore via the Minecraft state bucket"
}
