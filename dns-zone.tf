data "google_dns_managed_zone" "root" {
  name = var.dns_zone_name
}

resource "google_dns_record_set" "minecraft" {
  name = "minecraft.${data.google_dns_managed_zone.root.dns_name}"
  type = "A"
  ttl  = 60

  managed_zone = data.google_dns_managed_zone.root.name

  rrdatas = [
    google_compute_address.minecraft.address
  ]
}
