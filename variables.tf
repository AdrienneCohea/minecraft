variable "dns_zone_name" {}
variable "ssh_user" {}
variable "ssh_private_key" {}
variable "backup_bucket_name" {}
variable "backup_bucket_location" {}
variable "rcon_port" {
  default = 25575
}
variable "server_port" {
  default = 25565
}

variable "difficulty" {
  default = "easy"
}
variable "pvp" {
  default = false
}
variable "motd" {
  default = "A Minecraft Server"
}
variable "max_players" {
  default = 20
}

variable "machine_type" {
  default = "n2-standard-2"
}

variable "enable_whitelist" {
  default = true
}

variable "enforce_whitelist" {
  default = true
}

variable "server_maintainer" {
  type = object({
    name = string
    uuid = string
  })
}

variable "allowed_players" {
  default = []

  type = list(object({
    name = string
    uuid = string
  }))
}

variable "operators" {
  default = []

  type = list(object({
    name  = string
    uuid  = string
    level = number
  }))
}
