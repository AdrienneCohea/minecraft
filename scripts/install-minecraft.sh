#!/usr/bin/env bash

set -e

export DEBIAN_FRONTEND=noninteractive

sleep 15

sudo apt update -yq

sudo apt upgrade -y
sudo apt install -y git build-essential openjdk-8-jre-headless


MINECRAFT_HOME=/opt/minecraft
sudo useradd -r -m -U -d ${MINECRAFT_HOME} -s /bin/bash minecraft
sudo mkdir -p ${MINECRAFT_HOME}/{backups,tools,server}
mkdir tools
cd tools
git clone https://github.com/Tiiffi/mcrcon.git
cd mcrcon
gcc -std=gnu11 -pedantic -Wall -Wextra -O2 -s -o mcrcon mcrcon.c
cd ~
sudo mv -v ~/tools/* ${MINECRAFT_HOME}

# Download Minecraft
sudo wget https://launcher.mojang.com/v1/objects/bb2b6b1aefcd70dfd1892149ac3a215f6c636b07/server.jar -P ${MINECRAFT_HOME}/server

# Accept the EULA
echo "eula=true" | sudo tee ${MINECRAFT_HOME}/server/eula.txt

sudo mv --force /tmp/server.properties ${MINECRAFT_HOME}/server/server.properties
chmod +x /tmp/save-backup.sh
chmod +x /tmp/upload-backup.sh
sudo mv /tmp/save-backup.sh ${MINECRAFT_HOME}/tools/save-backup.sh
sudo mv /tmp/upload-backup.sh ${MINECRAFT_HOME}/tools/upload-backup.sh
sudo mv /tmp/banned-ips.json ${MINECRAFT_HOME}/server/
sudo mv /tmp/banned-players.json ${MINECRAFT_HOME}/server/
sudo mv /tmp/whitelist.json ${MINECRAFT_HOME}/server/
sudo mv /tmp/ops.json ${MINECRAFT_HOME}/server/

sudo chown -R minecraft:minecraft ${MINECRAFT_HOME}

sudo systemctl daemon-reload
sudo systemctl enable minecraft
sudo systemctl start minecraft

sudo systemctl reenable minecraft-backup.timer
sudo systemctl restart minecraft-backup.timer
