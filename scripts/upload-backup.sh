#!/bin/bash

## Get the name of the latest backup
latest_backup=$(sudo find /opt/minecraft/backups -type f -name '*.gz' -print0 | xargs -r -0 ls -1 -t | head -1)

if test -z "$${latest_backup}"; then
  echo "No backup found at /opt/minecraft/backups"
  exit 1
fi

## Save backup to cloud storage
gsutil cp $${latest_backup} gs://${backup_bucket}

## Remove local backups
sudo sudo find /opt/minecraft/backups/ -type f -name '*.gz' -delete
