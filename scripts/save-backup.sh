#!/bin/bash

function rcon {
  /opt/minecraft/mcrcon/mcrcon -H 127.0.0.1 -P ${rcon_port} -p ${rcon_password} "$1"
}

backup_filename=/opt/minecraft/backups/server-$(date +%F_%R).tar.gz

rcon "save-off"
rcon "save-all"
tar -cvpzf $${backup_filename} /opt/minecraft/server/world/*
rcon "save-on"

echo "Backup saved. The file is shown below:"
stat $${backup_filename}

sync
