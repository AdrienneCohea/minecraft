resource "google_compute_forwarding_rule" "minecraft" {
  name       = "minecraft"
  target     = google_compute_target_pool.minecraft.self_link
  port_range = tostring(var.server_port)

  load_balancing_scheme = "EXTERNAL"
  ip_protocol           = "TCP"

  ip_address = google_compute_address.minecraft.address
}

resource "google_compute_address" "minecraft" {
  name = "minecraft"
}


resource "google_compute_target_pool" "minecraft" {
  name = "minecraft"

  instances = [
    "us-west1-a/minecraft",
  ]
}
