resource "google_compute_instance" "minecraft" {
  name         = "minecraft"
  machine_type = var.machine_type

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
    }
  }

  network_interface {
    network = "default"

    access_config {
    }
  }

  service_account {
    email  = google_service_account.backup.email
    scopes = ["userinfo-email", "compute-ro", "storage-rw"]
  }

  connection {
    type        = "ssh"
    host        = self.network_interface.0.access_config.0.nat_ip
    user        = var.ssh_user
    private_key = file(var.ssh_private_key)
  }

  provisioner "file" {
    destination = "/tmp/minecraft.service"
    content = templatefile("${path.module}/templates/minecraft.service", {
      maximum_memory = local.machine_type_memory[var.machine_type] - 1024
      minimum_memory = local.machine_type_memory[var.machine_type] - 2048
      rcon_port      = var.rcon_port
      rcon_password  = random_password.rcon.result
    })
  }

  provisioner "file" {
    destination = "/tmp/server.properties"
    content = templatefile("${path.module}/templates/server.properties", {
      rcon_port     = var.rcon_port
      rcon_password = random_password.rcon.result
      server_port   = var.server_port
      pvp           = var.pvp
      difficulty    = var.difficulty
      motd          = var.motd
      max_players   = var.max_players
      
      enforce_whitelist = var.enforce_whitelist
      enable_whitelist  = var.enable_whitelist
    })
  }

  provisioner "file" {
    destination = "/tmp/save-backup.sh"
    content = templatefile("${path.module}/scripts/save-backup.sh", {
      rcon_port      = var.rcon_port
      rcon_password  = random_password.rcon.result
      backup_bucket  = google_storage_bucket.backup.name
    })
  }

  provisioner "file" {
    destination = "/tmp/upload-backup.sh"
    content = templatefile("${path.module}/scripts/upload-backup.sh", {
      backup_bucket  = google_storage_bucket.backup.name
    })
  }

  provisioner "file" {
    source      = "${path.module}/files/minecraft-backup.timer"
    destination = "/tmp/minecraft-backup.timer"
  }

  provisioner "file" {
    source      = "${path.module}/files/minecraft-backup.service"
    destination = "/tmp/minecraft-backup.service"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mv /tmp/*.service /etc/systemd/system",
      "sudo mv /tmp/*.timer /etc/systemd/system",
    ]
  }

  provisioner "file" {
    destination = "/tmp/banned-ips.json"
    content = templatefile("${path.module}/templates/banned-ips.json", {
    })
  }

  provisioner "file" {
    destination = "/tmp/banned-players.json"
    content = templatefile("${path.module}/templates/banned-players.json", {
    })
  }

  provisioner "file" {
    destination = "/tmp/whitelist.json"
    content = templatefile("${path.module}/templates/whitelist.json", {
      allowed_players = flatten([var.server_maintainer, var.allowed_players])
    })
  }

  provisioner "file" {
    destination = "/tmp/ops.json"
    content = templatefile("${path.module}/templates/ops.json", {
      operators = flatten([local.server_maintainer_as_operator, var.operators])})
  }

  provisioner "remote-exec" {
    scripts = [
      "${path.module}/scripts/install-minecraft.sh",
    ]
  }
}

locals {
  machine_type_memory = {
    "n2-standard-2" = 8192
  }

  server_maintainer_as_operator = { 
    name = var.server_maintainer.name,
    uuid = var.server_maintainer.uuid,
    level = 4
  }
}
